package com.desafio.Nitech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NitechApplication {

	public static void main(String[] args) {
		SpringApplication.run(NitechApplication.class, args);
	}

}
